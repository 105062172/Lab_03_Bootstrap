# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105062172    
- [ ] Name :洪翊訓
- [ ] URL :https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_marketing&stacked=h

## 10 Bootstrap elements (list in here)
1. 在header list上多加了一個drop down list (class="dropdown" <ul class="dropdown-menu">)
2. 
3. 中間改成放image(img src="105062172.jpg" class="img-circle")
4. 第一個圖片改成圓弧框 class="img-rounded"
5. 第二個圖片改成有邊框 class="img-thumbnail"
6. 將下方版面改成4等分 class="col-sm-3
7. 將Well改成lg class="well well-lg"
8. 將Well改成sm class="well well-sm"
9. 將footer的文字至中改掉改成至左  class="container-fluid"
10. 多加了一個dropup list class="dropup"> <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
